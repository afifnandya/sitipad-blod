const PurgecssPlugin = require("purgecss-webpack-plugin");
const glob = require("glob-all");
const path = require("path");

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/" : "/",
  productionSourceMap: false,
  configureWebpack: {
    // Merged into the final Webpack config
    plugins: [
      new PurgecssPlugin({
        paths: glob.sync([
          path.join(__dirname, "./src/index.html"),
          path.join(__dirname, "./**/*.vue"),
          path.join(__dirname, "./src/**/*.js")
        ]),
        whitelistPatterns: [
          /^collap/,
          /^dropdown/,
          /^nav/,
          /^tab/,
          /fade/,
          /^aos/,
          /^baguette/,
          /desc-bottom/,
          /desc-top/,
          /desc-right/,
          /desc-left/,
          /^bs-popover/,
          /show/,
          /^popover/,
          /arrow/,
          /^m/,
          /^p/
        ],
        whitelistPatternsChildren: [/^baguette/, /^popover/]
      })
    ]
  }
};
