import Vue from "vue";
import router from "./router";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import "@glidejs/glide/dist/css/glide.core.min.css";
import "@glidejs/glide/dist/css/glide.theme.min.css";
import Scrollspy from "vue2-scrollspy";
import VueMq from "vue-mq";
import App from "./App.vue";
import VueMeta from "vue-meta";

import {
  FontAwesomeIcon,
  FontAwesomeLayers
} from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faTwitter, faFacebookF } from "@fortawesome/free-brands-svg-icons";
import {
  faHome,
  faExternalLinkSquareAlt,
  faChevronLeft,
  faChevronRight,
  faSearch,
  faSpinner,
  faCircle
} from "@fortawesome/free-solid-svg-icons";
library.add(
  faHome,
  faExternalLinkSquareAlt,
  faChevronLeft,
  faChevronRight,
  faSearch,
  faSpinner,
  faTwitter,
  faFacebookF,
  faCircle
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("font-awesome-layers", FontAwesomeLayers);
Vue.config.productionTip = false;
Vue.use(VueMeta);
Vue.use(VueMq, {
  breakpoints: {
    sm: 576,
    md: 769,
    lg: 992,
    llg: 1200
  }
});
Vue.use(Scrollspy);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
