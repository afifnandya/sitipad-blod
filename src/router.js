import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("./components/MainPage.vue"),
    },
    {
      path: "/search",
      name: "search",
      component: () => import("./components/SearchPage.vue"),
    },
    {
      path: "/gallery",
      name: "gallery",
      component: () => import("./components/GalleryPage.vue"),
    },
    {
      path: "/cerita",
      name: "cerita",
      component: () => import("./components/CeritaPage.vue"),
    },
    {
      path: "/cerita/detail/belajar-dari-merenungi-kehidupan",
      name: "cerita/detail/belajar-dari-merenungi-kehidupan",
      props: true,
      component: () => import("./components/CeritaMerenungiKehidupan.vue"),
    },
    {
      path: "/cerita/detail/cara-berpikir-sempit",
      name: "cerita/detail/cara-berpikir-sempit",
      props: true,
      component: () => import("./components/CeritaBerpikirSempit.vue"),
    },
  ],
});
